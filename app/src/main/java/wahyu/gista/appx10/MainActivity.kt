package wahyu.gista.appx10

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.Request
import okhttp3.Response
import org.json.JSONArray

class MainActivity : AppCompatActivity() {

    lateinit var mhsAdapter : AdapterDataMhs
    var daftarMhs = mutableListOf<HashMap<String,String>>()
    var url = "http://192.168.43.68/app-x0a-web/show_data.php"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mhsAdapter = AdapterDataMhs(daftarMhs)
        listMhs.layoutManager = LinearLayoutManager(this)
        listMhs.adapter = mhsAdapter
        showDataMhs()
    }


    fun showDataMhs(){
        val request = StringRequest(
            com.android.volley.Request.Method.POST,url,
            com.android.volley.Response.Listener { response ->
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var mhs = HashMap<String,String>()
                    mhs.put("nim",jsonObject.getString("nim"))
                    mhs.put("nama",jsonObject.getString("nama"))
                    mhs.put("nama_prodi",jsonObject.getString("nama_prodi"))
                    mhs.put("url",jsonObject.getString("url"))
                    mhs.put("alamat",jsonObject.getString("alamat"))
                    mhs.put("kelamin",jsonObject.getString("kelamin"))
                    daftarMhs.add(mhs)
                }
                mhsAdapter.notifyDataSetChanged()
            },
            com.android.volley.Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_SHORT).show()
            })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}